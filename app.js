var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

//Import the mongoose module local connect
//var dbConfig = require('./db.js');
var mongoose = require('mongoose');

//Import the mongoose module cloud connect
mongoose.connect(
//  'mongodb+srv://Aaron:ykCvavcZJ5ikT3Gp@cluster0-ljrby.mongodb.net/test?retryWrites=true&w=majority'
  'mongodb://Aaron:ykCvavcZJ5ikT3Gp@cluster0-shard-00-00-ljrby.mongodb.net:27017,cluster0-shard-00-01-ljrby.mongodb.net:27017,cluster0-shard-00-02-ljrby.mongodb.net:27017/test?ssl=true&replicaSet=Cluster0-shard-0&authSource=admin&retryWrites=true&w=majority'
  ,
  {useNewUrlParser:true,
  useFindAndModify:false,
  useCreateIndex:true,
  useUnifiedTopology:true});

mongoose.connection.once('open',() => {
    console.log('connceted to database.')
    });

mongoose.Promise = global.Promise;
var db = mongoose.connection;

db.on('error', console.error.bind(console, 'MongoDB connection error:'));

// view engine setup 
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
